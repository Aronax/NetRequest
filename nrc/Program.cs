﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using nrc.Scripts;
using nrequest;
using static System.Console;

namespace nrc {
    internal class Program {
        private static void Main(string[] args) {
            if (args == null || args.Length < 1) {
                WriteLine("Argument is missing");
                return;
            }

            if (args[0].ToLower() == "class") {
                RunClass(new AsteriskAmi());


                Console.WriteLine("For exit press enter...");
                Console.ReadLine();
            } else {
                var source = args[0].StartsWith("http")
                    ? FromUrl(args[0])
                    : FromFile(args[0]);

                if (string.IsNullOrWhiteSpace(source)) {
                    WriteLine("Source is empty");
                    return;
                }
                RunScript(source);
            }
        }

        private static void RunClass(IScript script) {
            script.LogHandler = WriteLine;
            script.Run();
        }

        private static void RunScript(string source) {
            try {
                var providerOptions = new Dictionary<string, string> {
                    {"CompilerVersion", "v4.0"}
                };

                var provider = CodeDomProvider.CreateProvider("CSharp", providerOptions);

                var compilerParams = new CompilerParameters {GenerateInMemory = true, GenerateExecutable = false};

                compilerParams.ReferencedAssemblies.Add("System.Core.Dll");
                compilerParams.ReferencedAssemblies.Add("nrequest.dll");

                var results = provider.CompileAssemblyFromSource(compilerParams, source);
                if (results.Errors.HasErrors == false) {
                    var typeList = results.CompiledAssembly.GetTypes()
                        .Where(t => t.GetInterface("IScript") != null)
                        .ToList();

                    foreach (var script in typeList.Select(Activator.CreateInstance)
                        .OfType<IScript>()) {
                        script.LogHandler = WriteLine;
                        script.Run();
                    }
                } else {
                    WriteLine("Number of Errors: {0}", results.Errors.Count);
                    foreach (CompilerError err in results.Errors) {
                        WriteLine("ERROR {0}", err.ErrorText);
                    }
                }
            } catch (Exception ex) {
                WriteLine(ex);
            }
        }

        public static string FromFile(string filePath) {
            if (!Path.IsPathRooted(filePath)) {
                filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + filePath);
            }

            try {
                using (var sr = new StreamReader(filePath)) {
                    return sr.ReadToEnd();
                }
            } catch (Exception ex) {
                WriteLine(ex);
                return string.Empty;
            }
        }

        public static string FromUrl(string url) {
            try {
                using (var weblient = new NRequestWebClient()) {
                    return weblient.DownloadString(url);
                }
            } catch (Exception ex) {
                WriteLine(ex);
                return string.Empty;
            }
        }
    }
}