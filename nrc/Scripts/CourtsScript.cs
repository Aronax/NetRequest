﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using nrequest;

namespace nrc.Scripts {
    public class CourtsScript : BaseScript {
        private List<NRequestNode> _names = new List<NRequestNode>();

        public override void Run() {
            Encoding = Encoding.GetEncoding("windows-1251");

            var path = "/body/html/body/table[1]/tr/td/table/tr";

            var lines = Get("http://court.gov.ua/sudy/")
                .Html.Split("\r\n");

            var par = lines.Aggregate(string.Empty, (current, line) => line.StartsWith("<option value=")
                                                                           ? current + line + "</option>\r\n'"
                                                                           : current + line + "\r\n");
            var node = GetNode(par);

            _names = post("http://court.gov.ua/search_court.php", "q_court_search=суд&type_page=main_page")
                .select("ul/li/a");

            var str = node.Single(path + "/td/table[2]/tr/tr/td/script[contains(.,'obl1_2 = ')]").Text;
            str = str.Substring(0, str.IndexOf("//-->", StringComparison.Ordinal));
            str = str.Replace("<!--", "")
                     .Trim();

            //fill sub items
            var dict = new Dictionary<string, string>();

            lines = str.Split("\r\n");
            foreach (var pair in lines.Where(n => !n.Contains(" new Array();"))
                                      .Select(line => line.Split(" = "))) {
                dict[pair[0]] = pair[1].Replace("\";", "")
                                       .Replace("\"", "");
            }

            //Апеляційні суди
            var result = node.Select(path + "/td/table[2]/tr/tr/td[2]/table/tr[7]/td[1]/form[1]/select[1]/option")
                             .Select(itm => new Court(1, "Апеляційний", itm["value"], itm.Text))
                             .ToList();

            //Апеляційні господарські суди
            result.AddRange(node.Select(path + "/td/table[2]/tr/tr/td[2]/table/tr[7]/td[2]/form[1]/select[1]/option")
                                .Select(itm => new Court(3, "Апеляційний господарський", itm.Text, "", itm["value"])));

            //Апеляційні адміністративні суди
            result.AddRange(node.Select(path + "/td/table[2]/tr/tr/td[2]/table/tr[7]/td[3]/form[1]/select[1]/option")
                                .Select(itm => new Court(4, "Апеляційний адміністративний", itm.Atr("value"), "", itm.Text)));

            //Районні суди
            var rayoniOblForm = node.Single(path + "/td/table[2]/tr/tr/td[2]/table/tr[10]/td[1]/form[1]");
            foreach (var robl in rayoniOblForm.select("select[1]/option")) {
                result.AddRange(dict.Where(n => n.Key.StartsWith("obl1_" + robl["value"] + "["))
                                    .Select(itm => dict[itm.Key].Split(':'))
                                    .Select(spl => new Court(5, "Районний", spl[0], robl.Text, spl[1])));
            }

            //Місцеві господарські суди
            result.AddRange(node.Select(path + "[1]/td[1]/table[2]/tr/tr/td[2]/table/tr[10]/td[2]/form[1]/select[1]/option")
                                .Select(itm => new Court(10, "Місцевий господарський", itm.Atr("value"), itm.Text)));

            //Місцеві адміністративні суди
            result.AddRange(node.Select(path + "[1]/td[1]/table[2]/tr/tr/td[2]/table/tr[10]/td[3]/form[1]/select[1]/option")
                                .Select(itm => new Court(11, "Місцевий адміністративний", itm.Atr("value"), itm.Text)));

            //Міськрайонні суди
            foreach (var item in node.Select(path + "/td/table[2]/tr/tr/td[2]/table/tr[10]/td[1]/form[2]/select[1]/option")) {
                result.AddRange(dict.Where(n => n.Key.StartsWith("obl2_" + item["value"] + "["))
                                    .Select(n => dict[n.Key].Split(':'))
                                    .Select(n => new Court(6, "Міськрайонний", n[0], item.Text, n[1])));
            }

            //Міські
            foreach (var item in node.Select(path + "/td/table[2]/tr/tr/td[2]/table/tr[10]/td[1]/form[3]/select[1]/option")) {
                result.AddRange(dict.Where(n => n.Key.StartsWith("obl3_" + item["value"] + "["))
                                    .Select(n => dict[n.Key].Split(':'))
                                    .Select(n => new Court(7, "Міський", n[0], item.Text, "", n[1])));
            }

            //Міський районний
            foreach (var item in node.Select(path + "/td/table[2]/tr/tr/td[2]/table/tr[10]/td[1]/form[4]/select[1]/option")) {
                foreach (var spl in dict.Where(n => n.Key.StartsWith("mis1_" + item["value"] + "["))
                                        .Select(itm => dict[itm.Key].Split(':'))) {
                    result.AddRange(dict.Where(n => n.Key.StartsWith("raj1_" + spl[0] + "["))
                                        .Select(n => dict[n.Key].Split(':'))
                                        .Select(n => new Court(8, "Районний у місті", n[0], item.Text, "", spl[1], n[1])));
                }
            }

            var i = 0;
            foreach (var court in result) {
                LoadTax(court);
                log("{0} : {1}", i, court.Name);
                i++;
            }

            File.WriteAllText("courts.json", json(result));
        }

        private void LoadTax(Court item) {
            var taxDoc = post("http://court.gov.ua/sudytax/", "court_type=" + item.CourtType + "&reg_id=" + item.ID);

            if (taxDoc.Error)
                taxDoc = post("http://court.gov.ua/sudy/", "court_type=" + item.CourtType + "&reg_id=" + item.ID);

            var address  = taxDoc.Single("/html/body/table[1]/tr/td/table/tr[1]/td[2]/table/tr/td/table[1]/tr[2]/td[2]");
            var website  = taxDoc.Single("/html/body/table[1]/tr/td/table/tr[1]/td[2]/table/tr/td/table[1]/tr[4]/td[2]/a");
            var email    = taxDoc.Single("/html/body/table[1]/tr/td/table/tr[1]/td[2]/table/tr/td/table[1]/tr[6]/td[2]/a");
            var phones   = taxDoc.Single("/html/body/table[1]/tr/td/table/tr[1]/td[2]/table/tr/td/table[1]/tr[8]/td[2]");
            var whours   = taxDoc.Single("/html/body/table[1]/tr/td/table/tr[1]/td[2]/table/tr/td/table[2]");
            var taxtable = taxDoc.Single("/html/body/table[1]/tr/td/table/tr[1]/td[1]/table[2]/tr/tr/td[2]/table[1]");


            foreach (var name in _names) {
                var endUrl = name.Link.Replace("http://court.gov.ua/", "");
                if (taxDoc.ResponseUrl.Contains(endUrl))
                    item.Name = name.Text;
            }

            item.Taxurl = taxDoc.ResponseUrl;
            item.Address = address.Text;
            item.Website = website.Text;
            item.Email = email.Text;
            item.Phones = phones.Text;
            item.Whours = whours.Text;

            if (taxtable.Html.Trim() == "")
                return;

            item.Rec           = HtmlDecode(taxtable.Single("tr[1]/td").Text);
            item.RecCod        = HtmlDecode(taxtable.Single("tr[2]/td").Text);
            item.RecBank       = HtmlDecode(taxtable.Single("tr[3]/td").Text);
            item.RecBankCode   = HtmlDecode(taxtable.Single("tr[4]/td").Text);
            item.RecAccount    = HtmlDecode(taxtable.Single("tr[5]/td").Text);
            item.ClassOfBudget = HtmlDecode(taxtable.Single("tr[6]/td").Text);
            item.PayDest       = HtmlDecode(taxtable.Single("tr[7]/td").Text);
        }

        internal class Court {
            public Court() {}

            public Court(int courtType, string courtStr, string id, string obl, string rayon = "", string sity = "", string sityRayon = "", string taxurl = "", string address = "", string website = "", string email = "", string phones = "", string whours = "", string rec = "", string recCod = "", string recBank = "", string recBankCode = "", string recAccount = "", string classOfBudget = "", string payDest = "", string name = "") {
                CourtType     = courtType;
                CourtStr      = courtStr;
                ID            = id;
                Obl           = obl;
                Rayon         = rayon;
                Sity          = sity;
                SityRayon     = sityRayon;
                Taxurl        = taxurl;
                Address       = address;
                Website       = website;
                Email         = email;
                Phones        = phones;
                Whours        = whours;
                Rec           = rec;
                RecCod        = recCod;
                RecBank       = recBank;
                RecBankCode   = recBankCode;
                RecAccount    = recAccount;
                ClassOfBudget = classOfBudget;
                PayDest       = payDest;
                Name          = name;
            }

            public int CourtType { get; set; }
            public string CourtStr { get; set; }
            public string ID { get; set; }
            public string Obl { get; set; }
            public string Rayon { get; set; }
            public string Sity { get; set; }
            public string SityRayon { get; set; }
            public string Taxurl { get; set; }
            public string Address { get; set; }
            public string Website { get; set; }
            public string Email { get; set; }
            public string Phones { get; set; }
            public string Whours { get; set; }
            public string Rec { get; set; }
            public string RecCod { get; set; }
            public string RecBank { get; set; }
            public string RecBankCode { get; set; }
            public string RecAccount { get; set; }
            public string ClassOfBudget { get; set; }
            public string PayDest { get; set; }
            public string Name { get; set; }

            public override string ToString() {
                return string.Format("Address: {0}, ClassOfBudget: {1}, CourtStr: {2}, CourtType: {3}, Email: {4}, ID: {5}, Name: {6}, Obl: {7}, PayDest: {8}, Phones: {9}, Rayon: {10}, Rec: {11}, RecAccount: {12}, RecBank: {13}, RecBankCode: {14}, RecCod: {15}, Sity: {16}, SityRayon: {17}, Taxurl: {18}, Website: {19}, Whours: {20}", Address, ClassOfBudget, CourtStr, CourtType, Email, ID, Name, Obl, PayDest, Phones, Rayon, Rec, RecAccount, RecBank, RecBankCode, RecCod, Sity, SityRayon, Taxurl, Website, Whours);
            }
        }
    }
}