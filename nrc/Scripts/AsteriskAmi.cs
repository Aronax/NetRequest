﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using nrequest;

namespace nrc.Scripts {
    public class AsteriskAmi : BaseScript {
        private const string ResultFileName = "ami.json";
        private readonly AsteriskContext _context = new AsteriskContext();


        public override void Run() {
            Encoding = Encoding.UTF8;
            const string root = "https://wiki.asterisk.org";

            var baseUrl = $"{root}/wiki/display/AST/Asterisk";
            var commandTypes = new[] {"Events", "Actions", "Application"};
            //var commandTypes = new[] {"Application"};

            foreach (var version in new[] {11, 12, 13}.Select(n => _context.CreateVersion(n))) {
                foreach (var type in commandTypes) {
                    var url = type == "Application"
                        ? $"{baseUrl}+{version.Name}+Dialplan+Applications"
                        : $"{baseUrl}+{version.Name}+AMI+{type}";

                    foreach (var node in Get(url).Select("//*[@id='page-children']/span/a")) {
                        ProcessUrl(root + node.Link, version, Single(type));
                    }
                }
            }

            File.WriteAllText(ResultFileName, ToJsonIndented(_context));
        }


        public string Single(string text) {
            var result = text.EndsWith("s", StringComparison.InvariantCultureIgnoreCase) 
                ? text.Substring(0, text.Length - 1) 
                : text;

            return result;
        }

        public void ProcessUrl(string url, AsteriskVersion version, string type) {
            var doc = Get(url);

            var name = doc.Single("//*[@id='content']/div[@class='wiki-content']/h1");
            var synopsis = doc.Single("//*[@id='content']/div[5]/p[1]");
            var syntax = doc.Single("//*[@id='content']/div[5]/div/div/pre");

            if (syntax.NotEmpty) {
                var arguments = doc.Select("//*[@id='content']/div[5]/ul[1]/li")
                    .Aggregate("", (current, arg) => current + "\r\n" + arg.Text);

                var amiItem = _context.CreateItem(name.Text, synopsis.Text, syntax.Text, arguments, type);
                amiItem.ItemType = type;
                amiItem.Url = url;

                version.AmiItems.Add(amiItem);
                Log(amiItem.ToString());
            }
        }

        public class AsteriskContext {
            public Dictionary<string, AsteriskVersion> Versions { get; set; } = new Dictionary<string, AsteriskVersion>();

            public AsteriskVersion CreateVersion(int name) {
                var result = CreateVersion(name.ToString());
                return result;
            }

            public AsteriskVersion CreateVersion(string name) {
                var result = Versions[name] = new AsteriskVersion {Name = name};
                return result;
            }

            public AmiItem CreateItem(string name, string synopsis, string syntax, string arguments, string type) {
                var result = new AmiItem(name, synopsis, syntax, arguments, type);
                return result;
            }
        }

        public class AsteriskVersion {
            public string Name { get; set; }

            public List<AmiItem> AmiItems { get; set; } = new List<AmiItem>();
        }

        public class AmiItem {
            public AmiItem() { }

            public AmiItem(string name, string synopsis, string syntax, string arguments, string type) {
                Synopsis = synopsis;
                Name = name;
                PropertyName = name.ToCamel()
                    .Replace("-", "")
                    .Replace(" ", "")
                    .Replace("[", "")
                    .Replace("]", "")
                    .Replace("(", "")
                    .Replace(")", "");

                var syntaxLines = syntax.Split(new[] {"\r\n", "\n"}, StringSplitOptions.RemoveEmptyEntries);
                var argLines = arguments.Split(new[] {"\r\n", "\n"}, StringSplitOptions.RemoveEmptyEntries);


                if (type == "Application") {
                    syntax = syntax.Trim();
                    var startIndex = syntax.IndexOf('(') + 1;
                    var endIndex = syntax.LastIndexOf(')');

                    var syntaxArgs = syntax.Substring(startIndex, endIndex - startIndex);

                    var argLinesDesc = GetArgumentsWithDescriptions(argLines);

                    foreach (var item in syntaxArgs.Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries)) {
                        var argumentName = item.Trim();


                        var argument = new AmiArgument {
                            IsOptional = argumentName.StartsWith("["),
                            Name = item.Trim(' ', '[', ']')
                        };

                        if (argLinesDesc.ContainsKey(argument.Name)) {
                            argument.Description = argLinesDesc[argument.Name];
                        }

                        argument.FillPropertyName();

                        Items.Add(argument);
                    }
                } else {
                    foreach (var synt in syntaxLines.Select(
                        syntaxLine => syntaxLine.Split(new[] {": ", ":] "},
                            StringSplitOptions.RemoveEmptyEntries))
                        .Where(synt => synt.Length >= 2)) {
                        if (string.Equals(synt[1], name, StringComparison.CurrentCultureIgnoreCase)) {
                            ItemType = synt[0];
                        } else {
                            var itemName = synt[0];
                            var argument = new AmiArgument {
                                IsOptional = itemName.StartsWith("[")
                            };
                            itemName = itemName.Replace("[", "");
                            argument.Name = itemName;

                            var arg = argLines.FirstOrDefault(s => s.StartsWith(itemName));
                            if (arg != null) {
                                var argL = arg.Split(new[] {" - "}, StringSplitOptions.None);
                                if (argL.Length > 1) {
                                    argument.Description = argL[1];
                                }
                            }

                            if (Items.All(n => n.Name != argument.Name)) {
                                argument.FillPropertyName();
                                Items.Add(argument);
                            }
                        }
                    }
                }
            }

            public string ItemType { get; set; } = string.Empty;
            public string Name { get; set; }
            public string PropertyName { get; set; }
            public string Synopsis { get; set; }
            public string Url { get; set; }
            public List<AmiArgument> Items { get; set; } = new List<AmiArgument>();

            public override string ToString() {
                var result = new StringBuilder();

                result
                    .AppendLine($"{ItemType} - {Name}")
                    .AppendLine($"   Synopsis  : {Synopsis}")
                    .AppendLine($"   Url       : {Url}")
                    .AppendLine("   Arguments");

                foreach (var argument in Items) {
                    if (argument.Description.IsNotEmpty()) {
                        result.AppendLine($"     {argument.Name} - {argument.Description}");
                    } else {
                        result.AppendLine($"     {argument.Name}");
                    }
                }
                return result.ToString();
            }

            private Dictionary<string, string> GetArgumentsWithDescriptions(string[] lines) {
                var result = new Dictionary<string, string>();

                var name = string.Empty;

                foreach (var line in lines) {
                    if (line.StartsWith("\t")) {
                        if (line.IsNotEmpty()) {
                            result[name] += line.Replace("\t", "   ") + "\r\n";
                        }
                    } else {
                        var argL = line?.Split(new[] {" - "}, StringSplitOptions.None);

                        name = argL[0];
                        result[name] = "";

                        if (argL?.Length > 1) {
                            result[name] = argL[1];
                        }
                    }
                }

                return result;
            }
        }

        public class AmiArgument {
            public string Name { get; set; }
            public string PropertyName { get; set; }
            public string PropertyNameCamel => PropertyName.ToCamel();
            public string Type { get; set; } = "string";
            public string Description { get; set; } = string.Empty;
            public bool IsOptional { get; set; }


            public void FillPropertyName() {
                PropertyName = Name.ToCamel()
                    .Replace("-", "")
                    .Replace(" ", "")
                    .Replace("[", "")
                    .Replace("]", "")
                    .Replace("(", "")
                    .Replace(")", "");

                if (PropertyName.IsNotEmpty() && PropertyName.Length > 1) {
                    PropertyName = PropertyName[0].ToString().ToLower() + PropertyName.Remove(0, 1);
                }

                if (PropertyName == "interface") {
                    PropertyName = "@interface";
                }

                if (PropertyName == "event") {
                    PropertyName = "@event";
                }
            }
        }
    }
}