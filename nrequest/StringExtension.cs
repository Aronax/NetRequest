﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace nrequest {
    public static class StringExtension
    {
        public static bool IsEmpty(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }

        public static bool IsNotEmpty(this string str)
        {
            return string.IsNullOrWhiteSpace(str) == false;
        }

        public static string ToCamel(this string str)
        {
            if (str.IsEmpty())
                return str;

            var split = Regex.Replace(str, "([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))", "$1 ");

            split = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(split.ToLower());
            split = split.Replace(" ", "");
            return split;
        }

        public static string ToTitle(this string str)
        {
            return str.IsEmpty() ? str : CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }

        public static string Format(this string str, params object[] args)
        {
            return string.Format(str, args);
        }

        public static string Format(this string str, IFormatProvider provider, params object[] args)
        {
            return string.Format(provider, str, args);
        }
    }
}