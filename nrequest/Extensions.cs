﻿using System;

namespace nrequest
{
    public static class Extensions
    {
        public static string[] Split(this string source, string splitter, StringSplitOptions options = StringSplitOptions.None)
        {
            return source.IsEmpty()
                ? new string[] {}
                : source.Split(new[] {splitter}, options);
        }

        //public static bool IsEmpty(this string str)
        //{
        //    return string.IsNullOrWhiteSpace(str);
        //}

        //public static bool IsNotEmpty(this string str)
        //{
        //    return string.IsNullOrWhiteSpace(str) == false;
        //}
    }
}