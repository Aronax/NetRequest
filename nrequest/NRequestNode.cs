using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using HtmlAgilityPack;

namespace nrequest {
    public class NRequestNode {
        private readonly HtmlNode _node;
        private string _responseUrl = string.Empty;

        public NRequestNode(HtmlNode node) {
            _node = node;
        }

        public string ResponseUrl {
            get { return _responseUrl; }
            set { _responseUrl = value; }
        }

        public bool Error { get; set; }
        public int ErrorCode { get; set; }

        public bool Empty => _node == null;

        public bool NotEmpty => !Empty;

        public string Txt => _node.InnerText;

        public string Text => _node != null
            ? _node.InnerText
            : string.Empty;

        public string TrimText => _node?.InnerText.Trim() ?? string.Empty;

        public string Htm => _node != null
            ? _node.OuterHtml
            : string.Empty;

        public string Html => _node != null
            ? _node.OuterHtml
            : string.Empty;

        public string Link => GetAttributeValue(_node, "href");

        public string this[string key] => Atr(key);

        public string Atr(string name) {
            return GetAttributeValue(_node, name);
        }

        public static string GetAttributeValue(HtmlNode node, string attributeName) {
            var result = string.Empty;

            if (node == null)
                return result;

            if (node.Attributes.Count <= 0)
                return result;

            var atr = node.Attributes.FirstOrDefault(n => n.Name == attributeName);
            if (atr != null)
                result = atr.Value;
            return result;
        }

        #region single

        public NRequestNode O(string query) {
            return Single(query);
        }

        public NRequestNode One(string query) {
            return Single(query);
        }

        public NRequestNode Single(string query) {
            try {
                return new NRequestNode(_node.SelectSingleNode(query));
            }
            catch {
                // ignored
            }
            return null;
        }

        #endregion

        #region select

        public List<NRequestNode> S(string query) {
            return Select(query);
        }

        public List<NRequestNode> Sel(string query) {
            return Select(query);
        }

        public List<NRequestNode> select(string query) {
            return Select(query);
        }

        public List<NRequestNode> Select(string query) {
            var result = new List<NRequestNode>(0);
            try {
                result.AddRange(_node.SelectNodes(query).Select(itm => new NRequestNode(itm)));
            }
            catch {
                // ignored
            }
            return result;
        }

        #endregion

        public override string ToString()
        {
            return Text;
        }
    }
}