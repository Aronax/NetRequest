using System;
using System.Net;

namespace nrequest {
    public class NRequestWebClient : WebClient {
        public NRequestWebClient() {
            CookieContainer = new CookieContainer();
        }
        public Uri ResponseUri { get; private set; }
        public CookieContainer CookieContainer { get; set; }

        protected override WebResponse GetWebResponse(WebRequest request) {
            var response = base.GetWebResponse(request);
            if (response != null)
                ResponseUri = response.ResponseUri;
            ReadCookies(response);
            return response;
        }

        protected override WebRequest GetWebRequest(Uri address) {
            var request = base.GetWebRequest(address);
            var webRequest = request as HttpWebRequest;
            if (webRequest != null)
                webRequest.CookieContainer = CookieContainer;

            return request;
        }

        protected override WebResponse GetWebResponse(WebRequest request, IAsyncResult result) {
            var response = base.GetWebResponse(request, result);
            ReadCookies(response);

            return response;
        }

        private void ReadCookies(WebResponse r) {
            var response = r as HttpWebResponse;
            if (response == null)
                return;

            var cookies = response.Cookies;
            CookieContainer.Add(cookies);
        }
    }
}