using System;

namespace nrequest {
    public interface IScript
    {
        void Run();

        Action<string> OutHandler { get; set; }

        Action<string> LogHandler { get; set; }

        Action<Exception> ErrorHandler { get; set; }
    }
}