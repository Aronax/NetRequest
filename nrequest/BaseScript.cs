using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using HtmlAgilityPack;
using JetBrains.Annotations;
using Newtonsoft.Json;

namespace nrequest {
    public abstract class BaseScript : IScript {
        private Encoding _encoding = Encoding.UTF8;

        protected BaseScript() {
            HtmlNode.ElementsFlags["option"] = HtmlElementFlag.CanOverlap | HtmlElementFlag.Empty | HtmlElementFlag.Closed;
            HtmlNode.ElementsFlags["form"] = HtmlElementFlag.CanOverlap | HtmlElementFlag.Empty | HtmlElementFlag.Closed;
            HtmlNode.ElementsFlags["script"] = HtmlElementFlag.Closed;
            HtmlNode.ElementsFlags["input"] = HtmlElementFlag.Closed;
        }

        public virtual Encoding Encoding {
            get { return _encoding; }
            set { _encoding = value; }
        }

        public CookieContainer CookieContainer { get; set; } = new CookieContainer();

        public abstract void Run();
        public Action<string> OutHandler { get; set; }
        public Action<string> LogHandler { get; set; }
        public Action<Exception> ErrorHandler { get; set; }

        public NRequestNode Get(string url) {
            NRequestNode result = null;

            try {
                var client = new NRequestWebClient {
                    Proxy = null
                };

                ServicePointManager.MaxServicePoints = int.MaxValue;
                client.Encoding = _encoding;

                client.CookieContainer = CookieContainer;

                client.Headers.Add("user-agent", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)");

                if (ServicePointManager.ServerCertificateValidationCallback == null)
                    ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, errors) => true;


                var rawPage = client.DownloadString(url);


                var strings = rawPage.Split(new[] {"\r\n", "\n"}, StringSplitOptions.RemoveEmptyEntries);
                var sb = new StringBuilder();
                foreach (var itm in strings.Where(itm => !string.IsNullOrWhiteSpace(itm))) {
                    sb.Append(itm);
                    sb.Append("\r\n");
                }

                var document = new HtmlDocument();

                document.LoadHtml(sb.ToString());
                result = new NRequestNode(document.DocumentNode) {
                    ResponseUrl = client.ResponseUri.ToString()
                };
            }
            catch (Exception) {
                //RaseOnError(ex);
            }
            return result;
        }

        public async Task<NRequestNode> GetAsync(string url) {
            NRequestNode result = null;

            try {
                var client = new NRequestWebClient {
                    Proxy = null
                };

                ServicePointManager.MaxServicePoints = int.MaxValue;
                client.Encoding = _encoding;

                client.CookieContainer = CookieContainer;

                client.Headers.Add("user-agent", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)");

                if (ServicePointManager.ServerCertificateValidationCallback == null)
                    ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, errors) => true;

                var rawPage = await client.DownloadStringTaskAsync(url);

                var strings = rawPage.Split(new[] {"\r\n", "\n"}, StringSplitOptions.RemoveEmptyEntries);
                var sb = new StringBuilder();
                foreach (var itm in strings.Where(itm => !string.IsNullOrWhiteSpace(itm))) {
                    sb.Append(itm);
                    sb.Append("\r\n");
                }

                var document = new HtmlDocument();

                document.LoadHtml(sb.ToString());
                result = new NRequestNode(document.DocumentNode);

                if (client.ResponseUri != null)
                    result.ResponseUrl = client.ResponseUri.ToString();
            }
            catch (Exception) {
                //RaseOnError(ex);
            }
            return result;
        }

        public NRequestNode post(string url, string parameters) {
            return Post(url, parameters);
        }

        public NRequestNode Post(string url, string parameters) {
            NRequestNode result = null;
            var enc = Encoding.UTF8;
            var client = new NRequestWebClient {
                Proxy = null
            };
            try {
                ServicePointManager.MaxServicePoints = int.MaxValue;

                client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                client.Headers.Add("user-agent", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)");

                var uri = new Uri(url);
                client.Encoding = _encoding;
                client.CookieContainer = CookieContainer;

                if (ServicePointManager.ServerCertificateValidationCallback == null)
                    ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, errors) => true;

                enc = client.Encoding;
                var rawPage = client.UploadString(uri, parameters);

                var strings = rawPage.Split(new[] {"\r\n", "\n"}, StringSplitOptions.RemoveEmptyEntries);
                var sb = new StringBuilder();
                foreach (var itm in strings.Where(itm => !string.IsNullOrWhiteSpace(itm))) {
                    sb.Append(itm);
                    sb.Append("\r\n");
                }

                var document = new HtmlDocument();
                document.LoadHtml(sb.ToString());
                result = new NRequestNode(document.DocumentNode) {
                    ResponseUrl = client.ResponseUri.ToString()
                };
            }
            catch (Exception ex) {
                var webEx = ex as WebException;
                if (webEx == null)
                    return result;

                var doc = new HtmlDocument();
                var webResponse = webEx.Response as HttpWebResponse;

                var text = string.Empty;

                if (webResponse != null) {
                    using (var responseStream = webResponse.GetResponseStream()) {
                        if (responseStream != null && responseStream.CanRead) {
                            using (var reader = new StreamReader(responseStream, enc)) {
                                text = reader.ReadToEnd();
                            }
                        }
                    }
                }

                doc.LoadHtml(text);
                return webResponse != null
                           ? new NRequestNode(doc.DocumentNode) {ResponseUrl = webResponse.ResponseUri.ToString(), Error = true}
                           : new NRequestNode(doc.DocumentNode) {Error = true};
            }

            return result;
        }

        public async Task<NRequestNode> PostAsync(string url, string parameters) {
            NRequestNode result = null;
            var enc = Encoding.UTF8;
            var client = new NRequestWebClient {
                Proxy = null
            };
            try {
                ServicePointManager.MaxServicePoints = int.MaxValue;

                client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                var uri = new Uri(url);
                client.Encoding = _encoding;


                enc = client.Encoding;
                var rawPage = await client.UploadStringTaskAsync(uri, parameters);


                var strings = rawPage.Split(new[] {"\r\n", "\n"}, StringSplitOptions.RemoveEmptyEntries);
                var sb = new StringBuilder();
                foreach (var itm in strings.Where(itm => !string.IsNullOrWhiteSpace(itm))) {
                    sb.Append(itm);
                    sb.Append("\r\n");
                }

                var document = new HtmlDocument();
                document.LoadHtml(sb.ToString());
                result = new NRequestNode(document.DocumentNode) {
                    ResponseUrl = client.ResponseUri.ToString()
                };
            }

            catch (Exception ex) {
                var webEx = ex as WebException;
                if (webEx == null)
                    return result;

                var doc = new HtmlDocument();
                var webResponse = webEx.Response as HttpWebResponse;

                var text = string.Empty;

                if (webResponse != null) {
                    using (var responseStream = webResponse.GetResponseStream()) {
                        if (responseStream != null && responseStream.CanRead) {
                            using (var reader = new StreamReader(responseStream, enc)) {
                                text = reader.ReadToEnd();
                            }
                        }
                    }
                }

                doc.LoadHtml(text);
                return webResponse != null
                           ? new NRequestNode(doc.DocumentNode) {ResponseUrl = webResponse.ResponseUri.ToString(), Error = true}
                           : new NRequestNode(doc.DocumentNode) {Error = true};
            }

            return result;
        }

        public static string HtmlDecode(string s) {
            return HttpUtility.HtmlDecode(s);
        }

        public static string HtmlEncode(string s) {
            return HttpUtility.HtmlEncode(s);
        }

        [StringFormatMethod("format")]
        public virtual void log(string format, params object[] args) {
            Log(format, args);
        }

        [StringFormatMethod("format")]
        public virtual void Log(string format, params object[] args)
        {
            LogHandler?.Invoke(string.Format(format, args));
        }

        [StringFormatMethod("format")]
        public virtual void Out(string format, params object[] args)
        {
            OutHandler?.Invoke(string.Format(format, args));
        }

        public virtual void Out(string text)
        {
            OutHandler?.Invoke(text);
        }

        public virtual void Error(Exception ex)
        {
            ErrorHandler?.Invoke(ex);
        }

        public virtual void log(string text) {
            Log(text);
        }

        public virtual void Log(string text)
        {
            LogHandler?.Invoke(text);
        }

        public string FormatParameters(List<KeyValuePair<string, string>> items) {
            var result = items.Aggregate("", (current, condition) => current + "&" + condition.Key + "=" + condition.Value);

            return result.StartsWith("&") ? result.Substring(1) : result;
        }

        public string FormatParameters(Dictionary<string, string> items) {
            var result = items.Aggregate("", (current, condition) => current + "&" + condition.Key + "=" + condition.Value);
            return result;
        }

        public NRequestNode GetNode(string text) {
            var str = "<body>" + text + "</body>";
            var document = new HtmlDocument();
            document.LoadHtml(str);
            var result = new NRequestNode(document.DocumentNode);
            return result;
        }

        public string json(object obj) {
            return JsonConvert.SerializeObject(obj, Formatting.Indented);
        }

        public string JsonSerialize(object obj) {
            return JsonConvert.SerializeObject(obj, Formatting.Indented);
        }


        public string ToJson(object obj, bool indented = false)
        {
            return JsonConvert.SerializeObject(obj, indented ? Formatting.Indented : Formatting.None);
        }

        public string ToJsonIndented(object obj)
        {
            return ToJson(obj,true);
        }
    }
}